#!/usr/bin/python

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = """
module: table_info
short_description: Fetch tables from IP Fabric.
description: Fetch tables from IP Fabric.
author:
  - Alex Gittings (@minitriga)
extends_documentation_fragment:
  - community_fabric.ansible.provider
options:
  device:
    description: Hostname or IP of device
    type: str
  serial:
    description: Device serial number
    type: str
  sanitized:
    description: Mask passwords
    type: bool
  date:
    description: Date of configuration to retrieve
    type: list
  dest:
    description:
    - Absolute path where the file should be created to.
    - If C(src) is a directory, the hostname will be used for the filename.
    type: path
    required: yes
"""

EXAMPLES = """
- name: Get last configuration for L35ABC
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    device: L35ABC

- name: Get last configuration for L35ABC and save in a file
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    device: L35ABC
    dest: /tmp/config.txt

- name: Get previous configuration for L35ABC
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    device: L35ABC
    date: $prev # $last and $first supported

- name: Get configurations between specific dates for L35ABC
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    device: L35ABC
    date:
      - 11/22/ 1:30 #string or int supported
      - 1637629200

- name: Get all configurations for all devices
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    all: true

- name: Get all configurations for a L35ABC device
  community_fabric.ansible.configs:
    provider:
      base_url: "https://demo1.eu.ipfabric.io/"
      auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
    all: true
    device: L35ABC
"""

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.community_fabric.ansible.plugins.module_utils.module import AnsibleIPFModule
from ipfabric.tools import DeviceConfigs
import os
from ansible.utils.hashing import checksum, checksum_s
from ansible.module_utils._text import to_text


def handle_module(ipf):
    device = ipf.module.params['device']
    sn = ipf.module.params['sn']
    dest = ipf.module.params['dest']
    all = ipf.module.params['all']

    if dest and not device or sn:
        ipf.module.fail_json("device|sn parameter required when using dest parameter.")

    cfg = DeviceConfigs(ipf.ipf)

    if not all:
        if len(ipf.module.params['date']) > 2:
            ipf.module.fail_json("To many values specified for date. Specify either $last, $prev, $first or a list of start date and end date such as 11/22/ 1:30, 1637629200")
        elif len(ipf.module.params['date']) == 2:
            for date in ipf.module.params['date']:
                if date in ["$last", "$prev", "$first"]:
                    ipf.module.fail_json("When specifying a start and end date please specify a time and do not use $last, $prev or $first.")
            date = tuple(ipf.module.params['date'])
        elif len(ipf.module.params['date']) == 1:
            date = ipf.module.params['date'][0]

        config = cfg.get_configuration(device=device, date=date, sanitized=ipf.module.params['sanitized'], sn=sn)

        changed = False

        if not config:
            ipf.module.fail_json("Config could not be found.")

        if dest and config.text:
            if os.path.isdir(dest):
                tmp_hostname = config.hostname.replace("/", "-")
                dest = os.path.join(dest, f"{tmp_hostname}.cfg")

            if not os.path.exists(dest) or checksum(dest) != checksum_s(config.text):
                try:
                    if not ipf.module.check_mode:
                        with open(dest, "w") as output_file:
                            output_file.write(config.text)
                        changed = True
                except Exception as exc:
                    ipf.module.fail_json(f"Could not write to destination file {dest}: {to_text(exc)}")
        elif dest and not config.text:
            ipf.module.fail_json(f"Config not available for {config.hostname}")

        ipf.module.exit_json(changed=changed, data=config.dict())
    else:
        if dest:
            ipf.module.fail_json("dest parameter cannot be used with all.")
        if device or sn:
            configs = cfg.get_all_configurations(device=device, sn=sn)
        else:
            configs = cfg.get_all_configurations()

        if not configs:
            ipf.module.fail_json("Configurations not found.")
        else:
            formatted_configs = dict()
            for k, v in configs.items():
                formatted_configs[k] = list()
                for i in v:
                    formatted_configs[k].append(dict(i))

        ipf.module.exit_json(changed=False, data=formatted_configs)


def main():
    argument_spec = AnsibleIPFModule.provider_argument_spec()
    argument_spec.update(
        device=dict(
            type="str",
            required=False
        ),
        sn=dict(
            type="str",
            required=False
        ),
        date=dict(
            type="list",
            required=False,
            default=['$last']
        ),
        sanitized=dict(
            type="bool",
            required=False,
            default=True
        ),
        all=dict(
            type="bool",
            required=False,
            default=False
        ),
        dest=dict(
            type='path',
            required=False
        )
    )

    mutually_exclusive = [
        ('device', 'sn'),
    ]

    module = AnsibleModule(
          argument_spec=argument_spec,
          mutually_exclusive=mutually_exclusive,
          supports_check_mode=True
    )

    IPFHelper = AnsibleIPFModule(module)

    handle_module(IPFHelper)


if __name__ == "__main__":
    main()