
:orphan:

.. _list_of_collection_env_vars:

Index of all Collection Environment Variables
=============================================

The following index documents all environment variables declared by plugins in collections.
Environment variables used by the ansible-core configuration are documented in :ref:`ansible_configuration_settings`.

.. envvar:: ANSIBLE_INVENTORY_USE_EXTRA_VARS

    Merge extra vars into the available variables for composition (highest precedence).

    *Used by:*
    :ref:`community\_fabric.ansible.inventory inventory plugin <ansible_collections.community_fabric.ansible.inventory_inventory>`
.. envvar:: IPF_TOKEN

    IP Fabric API token to be able to gather device information.

    *Used by:*
    :ref:`community\_fabric.ansible.table\_info lookup plugin <ansible_collections.community_fabric.ansible.table_info_lookup>`
.. envvar:: IPF_URL

    Url of the IP Fabric API

    *Used by:*
    :ref:`community\_fabric.ansible.table\_info lookup plugin <ansible_collections.community_fabric.ansible.table_info_lookup>`
