
.. Document meta

:orphan:

.. |antsibull-internal-nbsp| unicode:: 0xA0
    :trim:

.. role:: ansible-attribute-support-label
.. role:: ansible-attribute-support-property
.. role:: ansible-attribute-support-full
.. role:: ansible-attribute-support-partial
.. role:: ansible-attribute-support-none
.. role:: ansible-attribute-support-na
.. role:: ansible-option-type
.. role:: ansible-option-elements
.. role:: ansible-option-required
.. role:: ansible-option-versionadded
.. role:: ansible-option-aliases
.. role:: ansible-option-choices
.. role:: ansible-option-choices-default-mark
.. role:: ansible-option-default-bold
.. role:: ansible-option-configuration
.. role:: ansible-option-returned-bold
.. role:: ansible-option-sample-bold

.. Anchors

.. _ansible_collections.community_fabric.ansible.configs_module:

.. Anchors: short name for ansible.builtin

.. Anchors: aliases



.. Title

community_fabric.ansible.configs module -- Fetch tables from IP Fabric.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. Collection note

.. note::
    This module is part of the `community_fabric.ansible collection <https://galaxy.ansible.com/community_fabric/ansible>`_ (version 0.0.8).

    To install it, use: :code:`ansible-galaxy collection install community\_fabric.ansible`.

    To use it in a playbook, specify: :code:`community_fabric.ansible.configs`.

.. version_added


.. contents::
   :local:
   :depth: 1

.. Deprecated


Synopsis
--------

.. Description

- Fetch tables from IP Fabric.


.. Aliases


.. Requirements






.. Options

Parameters
----------

.. rst-class:: ansible-option-table

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Comments

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-date"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-date:

      .. rst-class:: ansible-option-title

      **date**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-date" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`list` / :ansible-option-elements:`elements=string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Date of configuration to retrieve


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-dest"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-dest:

      .. rst-class:: ansible-option-title

      **dest**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-dest" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`path` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Absolute path where the file should be created to.

      If \ :literal:`src`\  is a directory, the hostname will be used for the filename.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-device"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-device:

      .. rst-class:: ansible-option-title

      **device**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-device" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Hostname or IP of device


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider:

      .. rst-class:: ansible-option-title

      **provider**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`dictionary` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Information used to connect to IP Fabric via API


      .. raw:: html

        </div>
    
  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider/api_version"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider/api_version:

      .. rst-class:: ansible-option-title

      **api_version**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider/api_version" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      The version of the IP Fabric REST API.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider/auth"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider/auth:

      .. rst-class:: ansible-option-title

      **auth**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider/auth" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      IP Fabric API auth token to be able to gather device information.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider/base_url"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider/base_url:

      .. rst-class:: ansible-option-title

      **base_url**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider/base_url" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Url of the IP Fabric API


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider/timeout"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider/timeout:

      .. rst-class:: ansible-option-title

      **timeout**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider/timeout" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Set HTTP Timeout


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-provider/verify"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-provider/verify:

      .. rst-class:: ansible-option-title

      **verify**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-provider/verify" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Allows connection when SSL certificates are not valid. Set to \ :literal:`false`\  when certificated are not trusted.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry-default:`true` :ansible-option-choices-default-mark:`← (default)`


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sanitized"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-sanitized:

      .. rst-class:: ansible-option-title

      **sanitized**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sanitized" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Mask passwords


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-serial"></div>

      .. _ansible_collections.community_fabric.ansible.configs_module__parameter-serial:

      .. rst-class:: ansible-option-title

      **serial**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-serial" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Device serial number


      .. raw:: html

        </div>


.. Attributes


.. Notes


.. Seealso


.. Examples

Examples
--------

.. code-block:: yaml+jinja

    
    - name: Get last configuration for L35ABC
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        device: L35ABC

    - name: Get last configuration for L35ABC and save in a file
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        device: L35ABC
        dest: /tmp/config.txt

    - name: Get previous configuration for L35ABC
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        device: L35ABC
        date: $prev # $last and $first supported

    - name: Get configurations between specific dates for L35ABC
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        device: L35ABC
        date:
          - 11/22/ 1:30 #string or int supported
          - 1637629200

    - name: Get all configurations for all devices
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        all: true

    - name: Get all configurations for a L35ABC device
      community_fabric.ansible.configs:
        provider:
          base_url: "https://demo1.eu.ipfabric.io/"
          auth: "{{ lookup('ansible.builtin.env', 'IPF_TOKEN')}}"
        all: true
        device: L35ABC




.. Facts


.. Return values


..  Status (Presently only deprecated)


.. Authors

Authors
~~~~~~~

- Alex Gittings (@minitriga)



.. Extra links

Collection links
~~~~~~~~~~~~~~~~

.. raw:: html

  <p class="ansible-links">
    <a href="https://gitlab.com/ip-fabric/integrations/ipfabric-ansible/-/issues" aria-role="button" target="_blank" rel="noopener external">Issue Tracker</a>
    <a href="https://gitlab.com/ip-fabric/integrations/ipfabric-ansible" aria-role="button" target="_blank" rel="noopener external">Repository (Sources)</a>
  </p>

.. Parsing errors

