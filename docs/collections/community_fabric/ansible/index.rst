


.. _plugins_in_community_fabric.ansible:

Community_Fabric.Ansible
========================

Collection version 0.0.8

.. contents::
   :local:
   :depth: 1

Description
-----------

Ansible Collection for managing IP Fabric

**Author:**

* IP Fabric <solution.architecture@ipfabric.io>

**Supported ansible-core versions:**

* 2.9.10 or newer

.. raw:: html

  <p class="ansible-links">
    <a href="https://gitlab.com/ip-fabric/integrations/ipfabric-ansible/-/issues" aria-role="button" target="_blank" rel="noopener external">Issue Tracker</a>
    <a href="https://gitlab.com/ip-fabric/integrations/ipfabric-ansible" aria-role="button" target="_blank" rel="noopener external">Repository (Sources)</a>
  </p>



.. toctree::
    :maxdepth: 1


Plugin Index
------------

These are the plugins in the community_fabric.ansible collection:


Modules
~~~~~~~

* :ref:`configs module <ansible_collections.community_fabric.ansible.configs_module>` -- Fetch tables from IP Fabric.
* :ref:`snapshot module <ansible_collections.community_fabric.ansible.snapshot_module>` -- Create, Update, Load, Unload or Delete Snapshots within IP Fabric
* :ref:`snapshot_info module <ansible_collections.community_fabric.ansible.snapshot_info_module>` -- Fetch Snapshot information within IP Fabric.
* :ref:`table_info module <ansible_collections.community_fabric.ansible.table_info_module>` -- Fetch tables from IP Fabric.

.. toctree::
    :maxdepth: 1
    :hidden:

    configs_module
    snapshot_module
    snapshot_info_module
    table_info_module


Inventory Plugins
~~~~~~~~~~~~~~~~~

* :ref:`inventory inventory <ansible_collections.community_fabric.ansible.inventory_inventory>` -- IP Fabric inventory source

.. toctree::
    :maxdepth: 1
    :hidden:

    inventory_inventory


Lookup Plugins
~~~~~~~~~~~~~~

* :ref:`table_info lookup <ansible_collections.community_fabric.ansible.table_info_lookup>` -- Queries and returns IP Fabric information.

.. toctree::
    :maxdepth: 1
    :hidden:

    table_info_lookup



.. seealso::

    List of :ref:`collections <list_of_collections>` with docs hosted here.
